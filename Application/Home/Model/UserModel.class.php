<?php
/**
 * Created by PhpStorm.
 * User: swumao
 * Date: 2016/6/5
 * Time: 22:29
 */

namespace Home\Model;

use Think\Model;
class UserModel extends Model
{
    /**
     * @param $name
     * @param $email
     * @param $tel
     * @param $password
     * @return mixed
     */
    public function register($name, $email, $tel, $password){
        $result = $this->add([
            'name'  =>  $name,
            'email' =>  $email,
            'tel'   =>  $tel,
            'password'  =>  $this->encrypt($password)
        ]);
        return $result;
    }

    /**
     * 根据用户名和密码找到对应的用户，如果存在则返回之，否则返回false
     * @param $username
     * @param $password
     * @return mixed
     */
    public function checkUser($username, $password)
    {
        $map['email|tel'] = $username;
        $user = $this->where($map)->find();
        if($user !== false){
            if($user['password'] == $this->encrypt($password)){
                return $user;
            }
        }
        return false;
    }

    public function updatePassword($userid, $password)
    {
        return $this->where(['id'=>$userid])->save(['password'=>$this->encrypt($password)]);
    }

    /**
     * @param $password
     * @return string
     */
    private function encrypt($password){
        return md5($password);
    }
}
