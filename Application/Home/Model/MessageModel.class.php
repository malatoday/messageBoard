<?php
namespace Home\Model;
use Think\Model;
/**
 * 留言内容表
 *
 * @return boolean 发布成功返回true，失败返回false
 */
class MessageModel extends Model{
    public function publish(array $data){
        $result = $this->add($data);
        if($result == false){
            return false;
        }else{
            return true;
        }
    }
}
