<?php
/**
 * @param int $timestamp
 * @return string
 */
function parse_show_datetime($timestamp)
{
    $now = time();
    if (($seconds = $now - $timestamp) < 0){
        return '未来';
    }
    if ($seconds < 60){
        return $seconds . '秒前';
    }
    if ($seconds < 60*60){
        return floor($seconds / 60) . '分钟前';
    }
    if ($seconds < 12*60*60){
        return floor($seconds / 60 / 12) . '小时前';
    }
    return date('Y年n月j日', $timestamp);
}